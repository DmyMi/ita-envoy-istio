# GKE
export PROJECT_ID=$(gcloud config get-value project)

gcloud container clusters create envoy-showcase --zone "europe-west1-b" \
--num-nodes 3 --machine-type=e2-medium \
--project=${PROJECT_ID} \
--scopes=gke-default,cloud-platform

kubectl apply -f envoy/00_setup/
export ENVOY_URL=`kubectl get svc envoy -o=jsonpath='{.status.loadBalancer.ingress[0].ip}'`

# EKS
export YOUR_NAME=your_name_here
$ cat << EOF > ekscfg.yaml
---
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: ${YOUR_NAME}-eks
  region: us-east-1

availabilityZones: ['us-east-1a', 'us-east-1b', 'us-east-1c']

managedNodeGroups:
- name: nodegroup
  desiredCapacity: 3
  instanceType: t2.small
  ssh:
    allow: false
cloudWatch:
  clusterLogging:
    enableTypes: ["api", "audit", "controllerManager"]
EOF

kubectl apply -f envoy/00_setup/
export ENVOY_URL=`kubectl get svc envoy -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}'`

# Envoy

kubectl apply -f 01_basic/
kubectl exec -it `kubectl get pods -l app=envoy -o jsonpath='{.items[0].metadata.name}'` -- less /etc/envoy/envoy.yaml

kubectl apply -f 02_config/
curl -v $ENVOY_URL:15001/headers

kubectl apply -f 03_timeout/
curl -v $ENVOY_URL:15001/headers

curl -v $ENVOY_URL:15000/stats

curl -v $ENVOY_URL:15000/stats | grep retry

kubectl apply -f 04_retries/
curl -v $ENVOY_URL:15001/status/500
curl -v $ENVOY_URL:15000/stats | grep retry

kubectl apply -f 05_circuit_breaking/

while true; do curl http://$ENVOY_URL:15001/ ; sleep 1; done
curl -v $ENVOY_URL:15001/

kubectl apply -f 06_shifting/
curl $ENVOY_URL:15001

kubectl apply -f 07_splitting/
curl $ENVOY_URL:15001

kubectl top pod `kubectl get pods -l app=envoy -o jsonpath='{.items[0].metadata.name}'` --containers 

# GKE
gcloud container clusters delete envoy-showcase --zone "europe-west1-b"

# EKS
eksctl delete cluster \
  --config-file=ekscfg.yaml